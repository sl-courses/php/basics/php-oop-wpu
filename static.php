<?php

class ContohStatic {
    public static $angka = 1;

    public static function halo() {
        //self digunakan untuk mengakses property atau method yang ada di dalam class itu sendiri
        //self::$angka++;
        //tidak pake this karena bukan property atau method dari object
        //sehingga tidak bisa menggunakan $this
        return "Halo." . self::$angka++ . "kali.";
    }
}
//Static Keyword
//Member yang terikat dengan class, bukan dengan object
//Nilai static akan tetap, tidak berubah, meskipun objectnya berubah
echo ContohStatic::$angka;
echo PHP_EOL;
echo ContohStatic::halo();
echo PHP_EOL;
echo ContohStatic::halo();

class Contoh {
    public static $angka = 1;

    public function halo() {
        return "Halo " . self::$angka++ . " kali." . PHP_EOL;
    }
}
Echo PHP_EOL . "Contoh kedua" . PHP_EOL;

$obj = new Contoh;
echo $obj->halo();
echo $obj->halo();
echo $obj->halo();

$obj2 = new Contoh;
echo $obj2->halo();
echo $obj2->halo();
echo $obj2->halo();


?>