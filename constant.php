<?php 

define('NAMA', 'Rizky');
echo "Define: " . NAMA;

class Coba {
    const NAMA = 'Dian';
}
echo PHP_EOL;
echo "Const: " . Coba::NAMA;

//Magic method of const
echo "Line : " . __LINE__;
echo PHP_EOL;
echo "File : " . __FILE__;
echo PHP_EOL;
echo "Dir : " . __DIR__;
echo PHP_EOL;
function sayHello() {
    return __FUNCTION__;
}
echo "Function : " . sayHello();
echo PHP_EOL;
class Coba2 {
    public $kelas = __CLASS__;
}
$obj = new Coba2;
echo "Class : " . $obj->kelas;
echo PHP_EOL;
function sayHello2() {
    return __METHOD__;
}
echo "Method : " . sayHello2();
echo PHP_EOL;
//Trait tidak bisa menggunakan magic method
//Trait adalah cara untuk mengelompokkan method-method yang sering digunakan
//__trait__ digunakan untuk mengakses nama trait
//nama trait adalah nama file trait
echo "Namespace : " . __NAMESPACE__;
echo PHP_EOL;


?>