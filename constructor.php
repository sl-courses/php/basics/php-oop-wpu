<?php 
class Produk {
    public $judul,
           $penulis,
           $penerbit,
           $harga;

    public function __construct($judul = "judul", $penulis = "penulis", $penerbit = "penerbit", $harga = 0) {
        $this->judul = $judul;
        $this->penulis = $penulis;
        $this->penerbit = $penerbit;
        $this->harga = $harga;
    }

    public function getLabel() {
        return "$this->penulis, $this->penerbit";
    }

}

$produk1 = new Produk("Naruto", "Masashi Kishimoto", "Shonen Jump", 30000);
$produk2 = new Produk("Uncharted", "Neil Druckmann", "Sony Computer", 250000);


echo "Komik : " . $produk1->getLabel() . PHP_EOL;
echo "Game : " . $produk2->getLabel() . PHP_EOL;

$produk3 = new Produk("Dragon Ball", "Akira Toriyama", "Shonen Jump", 30000);
echo "Komik : " . $produk3->getLabel() . PHP_EOL;

$produk4 = new Produk();
echo "Komik : " . $produk4->getLabel() . PHP_EOL;
?>